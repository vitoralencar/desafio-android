package com.desafiouol.service;

import com.desafiouol.model.Shot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by vitoralencars on 07/07/17.
 */

public interface RetrofitService {

    //Carregar shots
    @GET("shots")
    @Headers("Authorization: Bearer b114b1ca222480deef61b80207cc2e68acddc6547148985af744cf27a3d6deb3")
    Call<List<Shot>> getShots(@Query("page") int page, @Query("per_page") int perPage,
                              @Query("sort") String sort);

}
