package com.desafiouol.service;

/**
 * Created by vitoralencars on 07/07/17.
 */

public class BaseUrl {

    private static String dribbbleURL = "https://api.dribbble.com/v1/";

    public static String getDribbbleURL(){
        return dribbbleURL;
    }

}
