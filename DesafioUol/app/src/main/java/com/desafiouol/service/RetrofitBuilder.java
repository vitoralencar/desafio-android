package com.desafiouol.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vitoralencars on 07/07/17.
 */

public class RetrofitBuilder {

    private Retrofit retrofit;
    private RetrofitService service;

    public RetrofitBuilder(String baseURL){
        retrofit = initRetrofit(baseURL);
        service = initService(retrofit);
    }

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit initRetrofit(String baseURL){
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private RetrofitService initService(Retrofit retrofit){
        return retrofit.create(RetrofitService.class);
    }

    public RetrofitService getService(){
        return this.service;
    }

}
