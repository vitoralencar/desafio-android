package com.desafiouol.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.desafiouol.R;
import com.desafiouol.model.Shot;
import com.desafiouol.util.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by vitoralencars on 07/07/17.
 */

public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.ShotsViewHolder>{

    private List<Shot> shots;
    private Context context;
    private LayoutInflater layoutInflater;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListener;

    public ShotsAdapter(List<Shot> shots, Context context){
        this.shots = shots;
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ShotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_shot, parent, false);
        ShotsViewHolder myViewHolder = new ShotsViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ShotsViewHolder holder, int position) {

        setShotImage(shots.get(position).getImages().getNormal(), holder.ivShot);

        holder.tvViews.setText(Integer.toString(shots.get(position).getViewsCount()));
        holder.tvComments.setText(Integer.toString(shots.get(position).getCommentsCount()));
        holder.tvLikes.setText(Integer.toString(shots.get(position).getLikesCount()));

    }

    @Override
    public int getItemCount() {
        return shots.size();
    }

    private void setShotImage(String imageUrl, ImageView ivShot){
        Picasso.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .resize(800, 600)
                .into(ivShot);
    }

    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener recyclerview){
        mRecyclerViewOnClickListener = recyclerview;
    }

    public class ShotsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView ivShot;
        private TextView tvViews, tvComments, tvLikes;

        public ShotsViewHolder(View itemView) {
            super(itemView);

            ivShot = (ImageView)itemView.findViewById(R.id.iv_card_shot);
            tvViews = (TextView)itemView.findViewById(R.id.tv_views_number);
            tvComments = (TextView)itemView.findViewById(R.id.tv_comments_number);
            tvLikes = (TextView)itemView.findViewById(R.id.tv_likes_number);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onClickListener(view, getAdapterPosition());
            }
        }
    }
}
