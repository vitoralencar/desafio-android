package com.desafiouol.util;

import android.view.View;

/**
 * Created by vitoralencars on 09/07/17.
 */

public interface RecyclerViewOnClickListener {
    void onClickListener(View view, int position);
}
