package com.desafiouol.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.desafiouol.R;

/**
 * Created by vitoralencars on 08/07/17.
 */

public class CustomProgressDialog {

    private static ProgressDialog progressDialog;

    public static void start(Context context){
        progressDialog = new ProgressDialog(context, R.style.TransparentProgressDialogTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public static void stop(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
