package com.desafiouol.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Twitter;

/**
 * Created by vitoralencars on 09/07/17.
 */

public class DesafioUolApplication extends Application {

    private static final String SORT_PREFERENCES = "Application.SORT_PREFERENCES";
    private static final String DEFAULT_SORT_OPTION = "popular";

    private static Context instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Twitter.initialize(this);
        buildPicassoImageCache();
    }

    private void buildPicassoImageCache(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    private static SharedPreferences getSortPreferences(){
        return instance.getSharedPreferences(SORT_PREFERENCES, MODE_PRIVATE);
    }

    public static String getLastSort(){
        String lastSort = getSortPreferences().getString(SORT_PREFERENCES, null);
        return lastSort != null ? lastSort : DEFAULT_SORT_OPTION;
    }

    public static void setSortPreferences(String sortOption){
        SharedPreferences preferences = instance.getSharedPreferences(SORT_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SORT_PREFERENCES, sortOption);
        editor.commit();
    }

}
