package com.desafiouol.view;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.desafiouol.R;
import com.desafiouol.base.BaseActivity;
import com.desafiouol.model.Shot;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotDetailsActivity extends BaseActivity {

    private Shot shot;

    @BindView(R.id.iv_banner_shot)
    protected ImageView ivBannerShot;
    @BindView(R.id.tv_author)
    protected TextView tvAuthor;
    @BindView(R.id.tv_views_number_details)
    protected TextView tvViews;
    @BindView(R.id.tv_comments_number_details)
    protected TextView tvComments;
    @BindView(R.id.tv_likes_number_details)
    protected TextView tvLikes;
    @BindView(R.id.expandable_text_description)
    protected ExpandableTextView etvDescription;
    @BindView(R.id.ib_expand_description)
    protected ImageButton ibExpandDescription;
    @BindView(R.id.facebook_share)
    protected ImageView ivFacebookShare;
    @BindView(R.id.twitter_share)
    protected ImageView ivTwitterShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        initView();
    }

    private void initView(){
        shot = (Shot)getIntent().getSerializableExtra(SELECTED_SHOT);
        setShotBanner(shot.getImages().getNormal());
        setShotInfos();
    }

    private void setShotBanner(String imageUrl){
        Picasso.with(this)
                .load(imageUrl)
                .resize(1200,800)
                .into(ivBannerShot);
    }

    private void setShotInfos(){
        tvAuthor.setText(shot.getUser().getName());
        tvViews.setText(Integer.toString(shot.getViewsCount()));
        tvComments.setText(Integer.toString(shot.getCommentsCount()));
        tvLikes.setText(Integer.toString(shot.getLikesCount()));
        setDescriptionText();
        initExpandableButton();
        ivFacebookShare.setOnClickListener(facebookShotSharing());
        ivTwitterShare.setOnClickListener(twitterShotSharing());
    }

    private void setDescriptionText(){
        if(shot.getDescription() != null) {
            if (Build.VERSION.SDK_INT >= 24) {
                etvDescription.setText(Html.fromHtml(shot.getDescription(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                etvDescription.setText(Html.fromHtml(shot.getDescription()));
            }
        }else{
            ibExpandDescription.setVisibility(View.GONE);
        }
    }

    private void initExpandableButton(){
        ibExpandDescription.setOnClickListener(expandClick());
    }

    private View.OnClickListener expandClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etvDescription.isExpanded()){
                    ibExpandDescription.setRotation(0f);
                    etvDescription.collapse();
                }else{
                    ibExpandDescription.setRotation(180f);
                    etvDescription.expand();
                }
            }
        };
    }

    private View.OnClickListener facebookShotSharing(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog facebookShareDialog = new ShareDialog(ShotDetailsActivity.this);
                if(ShareDialog.canShow(ShareLinkContent.class)){
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(shot.getHtmlUrl()))
                            .build();
                    facebookShareDialog.show(linkContent);
                }
            }
        };
    }

    private View.OnClickListener twitterShotSharing(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TweetComposer.Builder tweetBuilder = new TweetComposer.Builder(ShotDetailsActivity.this)
                            .url(new URL(shot.getHtmlUrl()));
                    tweetBuilder.show();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        };
    }

}
