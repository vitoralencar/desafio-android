package com.desafiouol.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.desafiouol.R;
import com.desafiouol.adapter.ShotsAdapter;
import com.desafiouol.base.BaseActivity;
import com.desafiouol.model.Shot;
import com.desafiouol.util.RecyclerViewOnClickListener;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements RecyclerViewOnClickListener{

    private final static int PER_PAGE = 10;
    private final static String POPULAR = "popular";
    private final static String RECENT = "recent";
    private final static String VIEWS = "views";
    private final static String COMMENTS = "comments";

    private List<Shot> shots;
    private ShotsAdapter shotsAdapter;
    private int page;
    private String sort;
    private int lastPage;

    @BindView(R.id.rv_shots)
    protected RecyclerView rvShots;
    @BindView(R.id.swipe_refresh_shots)
    protected SwipyRefreshLayout swipeShots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initView();
        getShots(page, sort);
    }

    private void initView(){
        initRecyclerView();
        setDefaultShotsAttributes();
        swipeShots.setOnRefreshListener(swipeListener());
    }

    private void getShots(final int page, String sort){

        if(page == 1)
            showProgressDialog();

        if(page < lastPage) {
            getShotsService().getShots(page, PER_PAGE, sort).enqueue(new Callback<List<Shot>>() {
                @Override
                public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
                    if(response.body() != null && !response.body().isEmpty()) {
                        int shotsSize = shots.size();
                        shots.addAll(response.body());
                        loadMoreShots();
                        setScrollPosition(shotsSize);
                        stopProgressDialog();
                    }else{
                        lastPage = page;
                        swipeShots.setRefreshing(false);
                        swipeShots.setEnabled(false);
                    }
                }

                @Override
                public void onFailure(Call<List<Shot>> call, Throwable t) {
                    stopProgressDialog();
                    Toast.makeText(MainActivity.this, R.string.list_shots_error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setDefaultShotsAttributes(){
        page = 1;
        sort = getSortPreference();
        lastPage = Integer.MAX_VALUE;
    }

    private void initRecyclerView(){
        shots = new ArrayList<>();
        shotsAdapter = new ShotsAdapter(shots, this);
        shotsAdapter.setRecyclerViewOnClickListener(this);
        rvShots.setAdapter(shotsAdapter);
    }

    private void loadMoreShots(){
        shotsAdapter.notifyDataSetChanged();
        swipeShots.setRefreshing(false);
    }

    private void resetShotsListConfig(String newSort){
        shots.clear();
        page = 1;
        sort = newSort;
        swipeShots.setEnabled(true);
        lastPage = Integer.MAX_VALUE;
        getShots(page, sort);
    }

    private SwipyRefreshLayout.OnRefreshListener swipeListener(){
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                page++;
                getShots(page, sort);
            }
        };
    }

    private void setScrollPosition(int shotSize){
        LinearLayoutManager layoutManager = (LinearLayoutManager)rvShots.getLayoutManager();
        layoutManager.scrollToPosition(shotSize + 2);
        rvShots.setLayoutManager(layoutManager);
    }

    private MenuItem.OnMenuItemClickListener showSortOptions(){
        return new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                createAlertSortOptions();
                return false;
            }
        };
    }

    private void createAlertSortOptions(){
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View sortView = inflater.inflate(R.layout.dialog_sort, null);

        AlertDialog dialog;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);

        dialogBuilder.setView(sortView);

        final RadioGroup rgSortOptions = (RadioGroup)sortView.findViewById(R.id.rg_sort_options);
        RadioButton rbPopular = (RadioButton)sortView.findViewById(R.id.rb_sort_popular);
        RadioButton rbRecent = (RadioButton)sortView.findViewById(R.id.rb_sort_recent);
        RadioButton rbViews = (RadioButton)sortView.findViewById(R.id.rb_sort_views);
        RadioButton rbComments = (RadioButton)sortView.findViewById(R.id.rb_sort_comments);

        setSortOptionChecked(rbPopular, rbRecent, rbViews, rbComments);

        dialogBuilder.setTitle(R.string.sort_dialog_title);

        dialogBuilder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sortShots(rgSortOptions);
                dialog.dismiss();
            }
        });

        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    private void setSortOptionChecked(RadioButton popular, RadioButton recent, RadioButton views, RadioButton comments){
        switch (sort){
            case POPULAR:
                popular.setChecked(true);
                break;
            case RECENT:
                recent.setChecked(true);
                break;
            case VIEWS:
                views.setChecked(true);
                break;
            case COMMENTS:
                comments.setChecked(true);
                break;
        }
    }

    private void sortShots(RadioGroup options){
       switch (options.getCheckedRadioButtonId()){
           case R.id.rb_sort_popular:
               if(!isCurrentSortShots(POPULAR)) {
                   resetShotsListConfig(POPULAR);
                   setSortPreference(POPULAR);
               }
               break;
           case R.id.rb_sort_recent:
               if(!isCurrentSortShots(RECENT)) {
                   resetShotsListConfig(RECENT);
                   setSortPreference(RECENT);
               }
               break;
           case R.id.rb_sort_views:
               if(!isCurrentSortShots(VIEWS)) {
                   resetShotsListConfig(VIEWS);
                   setSortPreference(VIEWS);
               }
               break;
           case R.id.rb_sort_comments:
               if(!isCurrentSortShots(COMMENTS)) {
                   resetShotsListConfig(COMMENTS);
                   setSortPreference(COMMENTS);
               }
               break;
       }
    }

    private boolean isCurrentSortShots(String sortOption){
        return sortOption.equals(sort);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem sortMenuItem = menu.findItem(R.id.menu_shots_sort);
        sortMenuItem.setOnMenuItemClickListener(showSortOptions());

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(MainActivity.this, ShotDetailsActivity.class);
        intent.putExtra(SELECTED_SHOT, shots.get(position));
        startActivity(intent);
    }
}
