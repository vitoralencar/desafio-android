package com.desafiouol.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.desafiouol.util.DesafioUolApplication;
import com.desafiouol.service.BaseUrl;
import com.desafiouol.service.RetrofitBuilder;
import com.desafiouol.service.RetrofitService;
import com.desafiouol.util.CustomProgressDialog;

/**
 * Created by vitoralencars on 08/07/17.
 */

public class BaseActivity extends AppCompatActivity {

    protected final static String SELECTED_SHOT = "SELECTED_SHOT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showProgressDialog(){
        CustomProgressDialog.start(this);
    }

    protected void stopProgressDialog(){
        CustomProgressDialog.stop();
    }

    protected RetrofitService getShotsService(){
        RetrofitBuilder builder = new RetrofitBuilder(BaseUrl.getDribbbleURL());
        return builder.getService();
    }

    protected String getSortPreference(){
        return DesafioUolApplication.getLastSort();
    }

    protected void setSortPreference(String sortOption){
        DesafioUolApplication.setSortPreferences(sortOption);
    }
}
